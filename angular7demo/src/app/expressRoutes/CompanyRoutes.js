// companyRoutes.js

var express = require('express');
var app = express();
var companyRoutes = express.Router();

// Require Item model in our routes module
var Company = require('../models/Company');

// Defined store route
companyRoutes.route('/add').post(function (req, res) {
	var company = new Company(req.body);
	company.save()
    .then(item => {
    	res.status(200).json({'company': 'Company added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
companyRoutes.route('/').get(function (req, res) {
	Company.find(function (err, companys){
		if(err){
			console.log(err);
		}
		else {
			res.json(companys);
		}
	});
});

// Defined edit route
companyRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Company.findById(id, function (err, company){
		res.json(company);
	});
});

//  Defined update route
companyRoutes.route('/update/:id').post(function (req, res) {
	Company.findById(req.params.id, function(err, company) {
		if (!company)
			return next(new Error('Could not load a Company Document using id ' + req.params.id));
		else {
            company.name = req.body.name;
            company.Employees = req.body.Employees;
            company.Address = req.body.Address;
            company.Type = req.body.Type;

			company.save().then(company => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
companyRoutes.route('/delete/:id').get(function (req, res) {
   Company.findOneAndDelete({_id: req.params.id}, function(err, company){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Company + ' using id ' + req.params.id );
    });
});

module.exports = companyRoutes;