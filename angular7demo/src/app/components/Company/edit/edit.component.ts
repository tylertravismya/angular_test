import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from '../../../services/Company.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Company/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditCompanyComponent extends SubBaseComponent implements OnInit {

  company: any;
  companyForm: FormGroup;
  title = 'Edit Company';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: CompanyService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.companyForm = this.fb.group({
      name: ['', Validators.required],
      Employees: ['', ],
      Address: ['', ],
      Type: ['', ]
   });
  }
  updateCompany(name, Employees, Address, Type) {
    this.route.params.subscribe(params => {
    	this.service.updateCompany(name, Employees, Address, Type, params['id'])
      		.then(success => this.router.navigate(['/indexCompany']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.company = this.service.editCompany(params['id']).subscribe(res => {
        this.company = res;
      });
    });
    
    super.ngOnInit();
  }
}
